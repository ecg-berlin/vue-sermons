<?php

use yii\db\Migration;

class m170531_065204_languages_to_language extends Migration
{
    public function safeUp()
    {
        $this->addColumn('sermon', 'language', $this->string());
        $russ = ["А","Б","В","Г","Д","Ђ","Е","Ѐ","Ё","Є","Ж","З","И","Ѝ","Н","О","П","Р","С","Х","Ш","Щ","Ъ","Ы","Ь","Э","Ю","Я",
        "м","М", "р","о","и","п","у","ь","к","х"];

        foreach(\app\models\Sermon::find()->each() as $sermon) {
            /** @var \app\models\Sermon $sermon */
            if(!empty($sermon->languages)) {
                $sermon->language = $sermon->languages[0];
            } else {
                if($this->contains($sermon->title, $russ)) {
                    $sermon->language = "ru";
                } else {
                    $sermon->language = "de";
                }
            }
            $sermon->save(false);
        }
    }

    private function  contains($str, $arr)
    {
        foreach($arr as $a) {
         if (stripos($str,$a) !== false) return true;
        }
        return false;
    }

    public function safeDown()
    {
        $this->dropColumn('sermon', 'language', $this->string());
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170531_065204_languages_to_language cannot be reverted.\n";

        return false;
    }
    */
}
