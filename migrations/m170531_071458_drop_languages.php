<?php

use yii\db\Migration;

class m170531_071458_drop_languages extends Migration
{
    public function safeUp()
    {
        $this->dropColumn("sermon", "languagesJson");
    }

    public function safeDown()
    {
        $this->addColumn('sermon', 'languagesJson', $this->string());

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170531_071458_drop_languages cannot be reverted.\n";

        return false;
    }
    */
}
