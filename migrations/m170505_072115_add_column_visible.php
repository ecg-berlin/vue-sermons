<?php

use yii\db\Migration;

class m170505_072115_add_column_visible extends Migration
{
    public function safeUp()
    {
        $this->addColumn('sermon', 'hidden', $this->integer());
        foreach(\app\models\Sermon::find()->each() as $sermon) {
            $sermon->hidden = false;
            $sermon->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropColumn('sermon', 'hidden');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170505_072115_add_column_visible cannot be reverted.\n";

        return false;
    }
    */
}
