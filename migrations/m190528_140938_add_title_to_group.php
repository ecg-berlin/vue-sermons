<?php

use yii\db\Migration;

/**
 * Class m190528_140938_add_title_to_group
 */
class m190528_140938_add_title_to_group extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn("group", "titlesJson", $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn("group", "titlesJson");
    }
}
