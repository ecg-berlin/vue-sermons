<?php

use yii\db\Migration;

class m171018_130439_add_youtube_field extends Migration
{
    public function safeUp()
    {
        $this->addColumn('sermon', 'youtube', $this->text());

    }

    public function safeDown()
    {
       $this->dropColumn('sermon', 'youtube');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171018_130439_add_youtube_field cannot be reverted.\n";

        return false;
    }
    */
}
