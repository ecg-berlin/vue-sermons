<?php

use yii\db\Migration;

/**
 * Class m201117_100332_show_only_direct
 */
class m201117_100332_show_only_direct extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn("sermon", "showOnlyDirect", $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn("sermon", "showOnlyDirect");
    }

  
    
}
