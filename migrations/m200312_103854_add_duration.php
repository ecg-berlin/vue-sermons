<?php

use yii\db\Migration;

/**
 * Class m200312_103854_add_duration
 */
class m200312_103854_add_duration extends Migration
{
    public function up()
    {
        $this->addColumn("sermon", "duration", $this->integer());
    }


    public function down()
    {
        $this->dropColumn("sermon", "duration");
    }

  
}
