#!/bin/bash
set -e
wget https://github.com/sass/dart-sass/releases/download/1.32.12/dart-sass-1.32.12-linux-x64.tar.gz
tar -zxvf dart-sass-1.32.12-linux-x64.tar.gz 
cp dart-sass/* /usr/local/bin -r