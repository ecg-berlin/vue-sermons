#!/bin/bash
set -e
sudo service docker start
sudo docker build -t registry.gitlab.com/ecg-berlin/vue-sermons/php-base .
sudo docker push registry.gitlab.com/ecg-berlin/vue-sermons/php-base

