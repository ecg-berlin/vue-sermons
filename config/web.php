<?php
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'sermons',
    'name' => 'Sermons',
    'language' => 'de',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                [
                    'pattern' => 'feed/<groupCode>/<language>',
                    'route' => 'sermon/feed',
                    'defaults' => ['groupCode' => "", 'language' => ''],
                ],
            ],
        ],
        'cache'   => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => 'localhost',
                'port' => 6379,
                'database' => 0,
            ],
            'keyPrefix'    => substr(md5('memCache'.$params['secret'].'a'), 0, 8),
        ],
        'request' => [
            'class' => '\yii\web\Request',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'cookieValidationKey' => 'OjciaU'.$params['secret'],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'authTimeout' => 60 * 60 * 24 * 30, //30 days,
            'loginUrl' => ['user/login'],
        ],
        'response' => [
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => true,
                ],
            ],
        ],
        'datadog' => [
            'class' => 'app\components\Datadog',
            'apiKey' => $params['datadog']['apiKey'] ?? '',
        ],
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                [
                    'class' => 'yii\log\SyslogTarget',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                    ],

                ],
                [
                    'class' => 'app\components\DatadogTarget',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                    ],
                    'tags' => ['sermon'],
                ],

            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];


if (YII_DEBUG === true) {
    $debug = require(__DIR__ . '/debug.php');
    $config = \yii\helpers\ArrayHelper::merge($config, $debug);
    $config['components']['log']['targets'][] = [
        'class' => 'yii\log\FileTarget',
        'levels' => ['info']
    ];
}
return $config;
