<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname={{DB_NAME}}',
    'username' => '{{USERNAME}}',
    'password' => '{{PASSWORD}}',
    'charset' => 'utf8',
    'enableSchemaCache' => false,
    'schemaCacheDuration' => 3600,
    'schemaCache' => 'cache'
];
