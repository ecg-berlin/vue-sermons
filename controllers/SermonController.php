<?php

namespace app\controllers;

use app\helpers\Feed;
use app\helpers\Scripture;
use app\models\Group;
use app\models\Sermon;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SermonController extends JsonController
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['rss', 'feed'],
                'formats' => [
                    'application/xml' => Response::FORMAT_RAW,
                    'application/rss+xml' => Response::FORMAT_RAW,
                    'text/xml' => Response::FORMAT_RAW,
                    'text/html' => Response::FORMAT_RAW,
                ],
            ],
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['hit', 'list', 'create', 'add', 'series', 'info', 'languages', 'speakers', 'series-data'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    'text/xml' => Response::FORMAT_JSON,
                    'text/html' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    public function actionIndex($id)
    {
        $sermon = Sermon::find()->where(['id' => $id])->andWhere(['hidden' => false])->one();
        if ($sermon === null) {
            throw new NotFoundHttpException('The requested sermon does not exist.');
        }
        $this->view->params['feedUrl'] = aurl(['sermon/feed', 'groupCode' => $sermon->group->code, 'language' => ""]);
        $this->view->title = $sermon->title;
        return $this->render('sermon', ['sermon' => $sermon]);
    }

    /**
     * @param $id
     * @return array
     * old url: /sermon/hit/#SermonId UpdateDownloadCounterR GET
     */
    public function actionHit($id)
    {
        $sermon = $this->findModel($id);
        if (empty($sermon->hits)) {
            $sermon->hits = 1;
        } else {
            $sermon->hits++;
        }
        $sermon->save();
        return ['response' => 'ok'];
    }

    /**
     * @return array
     */
    public function actionList()
    {
        $sermons = $this->getQuery();
        return $this->sermonList($sermons);
    }



    /**
     * @return string
     * @throws NotFoundHttpException, \Exception
     */
    public function actionFeed($groupCode = "", $language = "")
    {
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');

        $sermons = Sermon::find()->joinWith('group')
            ->andWhere(['<>', 'hidden', 1])
            ->andWhere(['<=', 'date', $today])
            ->limit(1000);
        $sermons = $sermons->andWhere(['or', ['<>', 'showOnlyDirect', 1], ['showOnlyDirect' => NULL]]);

        $groupName = "";

        if (!empty($groupCode)) {
            $group = Group::findOne(['code' => $groupCode]);
            if (!$group) {
                throw new NotFoundHttpException("group not found");
            }
            $groupName = $group->name;
            $groupCode = $group->code;
            $sermons = $sermons->andWhere(['group.code' => $groupCode]);
        }
        if (!empty($language)) {
            $language = substr($language, 0, 2);
            $sermons = $sermons->andWhere(['sermon.language' => $language]);
        }
        return Feed::createRssFeed($this, $sermons, $groupName, $groupCode, $language);
    }


    /**
     * @return string
     * @throws NotFoundHttpException, \Exception
     */
    public function actionRss()
    {
        $sermons = $this->getQuery()->limit(1000);
        $sermons = $sermons->andWhere(['or', ['<>', 'showOnlyDirect', 1], ['showOnlyDirect' => NULL]]); //always hide
        $groupName = "";
        $language = "";

        $groupCode = "";
        if (isset($_REQUEST['filter']) && isset($_REQUEST['filter']['groupCode']) && !empty($_REQUEST['filter']['groupCode'])) {
            $group = Group::findOne(['code' => $_REQUEST['filter']['groupCode']]);
            if (!$group) {
                throw new NotFoundHttpException("group not found");
            }
            $groupName = $group->name;
            $groupCode = $group->code;
        }
        if (isset($_REQUEST['filter']) && isset($_REQUEST['filter']['language'])) {
            $language = substr($_REQUEST['filter']['language'], 0, 2);
        }
        return Feed::createRssFeed($this, $sermons, $groupName, $groupCode, $language);
    }

    /**
     * @return ActiveQuery
     * @throws \Exception
     */
    private function getQuery()
    {
        $filter = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : [];
        $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : [];
        $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 0;
        $offset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;
        $showDirect = isset($_REQUEST['showDirect']) ? intval($_REQUEST['showDirect']) : 0;

        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');

        $sermons = Sermon::find()->joinWith('group')
            ->andWhere(['<>', 'hidden', 1])
            ->andWhere(['<=', 'date', $today]);
        if($showDirect == 0) {
            $sermons = $sermons->andWhere(['or', ['<>', 'showOnlyDirect', 1], ['showOnlyDirect' => NULL]]);
        }

        if (isset($filter['groupCode']) && !empty($filter['groupCode'])) {
            $sermons = $sermons->andWhere(['group.code' => $filter['groupCode']]);
        }
        if (isset($filter['seriesName']) && !empty($filter['seriesName'])) {
            if(contains(" ", $filter['seriesName'])) {
                $sermons = $sermons->andWhere(['sermon.seriesName' => $filter['seriesName']]);
            } else {
                $sermons = $sermons->andWhere(['like', 'sermon.seriesName', $filter['seriesName']]);
            }
           
        }
        if (isset($filter['title']) && !empty($filter['title'])) {
            $sermons = $sermons->andWhere(['like', 'sermon.title', $filter['title']]);
        }
        if (isset($filter['speaker']) && !empty($filter['speaker'])) {
            $sermons = $sermons->andWhere(['like', 'sermon.speaker', $filter['speaker']]);
        }
        if (isset($filter['id'])) {
            $sermons = $sermons->andWhere(['sermon.id' => $filter['id']]);
        }
        if (isset($filter['language']) && !empty($filter['language'])) {
            $sermons = $sermons->andWhere(['sermon.language' => $filter['language']]);
        }
        if (isset($filter['all'])) {
            $sermons = $sermons->andFilterWhere([
                'or',
                ['like', 'seriesName', $filter['all']],
                ['like', 'title', $filter['all']],
                ['like', 'speaker', $filter['all']]
            ]);
        }
        if ($limit != 0) {
            $sermons = $sermons->limit($limit);
        }

        if ($offset != 0) {
            $sermons = $sermons->offset($offset);
        }

        if (!empty($sort) && is_array($sort)) {
            $s = [];
            foreach ($sort as $key => $order) {
                if ($order === "asc") {
                    $s[$key] = SORT_ASC;
                } else {
                    $s[$key] = SORT_DESC;
                }
            }
            $s['title'] = SORT_ASC;
            $sermons = $sermons->orderBy($s);
        } else {
            $sermons = $sermons->orderBy(['date' => SORT_DESC, 'title' => SORT_ASC]);
        }
        return $sermons;
    }

    /**
     * @param ActiveQuery $query
     * @return array
     */
    private function sermonList($query)
    {
        $ret = [];
        foreach ($query->each() as $sermon) {
            /** @var Sermon $sermon */
            if (empty($sermon->files['image'])) {
                $images = [$sermon->getAlternativeCover()];
            } else {
                $images = $sermon->files['image'];
            }

            $item = [
                'id' => $sermon->id,
                'title' => $sermon->title,
                'audio' => $this->addWebProtocol($sermon->files['audio']),
                'video' => $this->addWebProtocol($sermon->files['video']),
                'other' => $this->addWebProtocol($sermon->files['other']),
                'image' => $this->addWebProtocol($images),
                'speaker' => $sermon->speaker,
                'seriesName' => empty($sermon->seriesName) ? "" : $sermon->seriesName,
                'date' => \Yii::$app->formatter->asDate($sermon->date, 'short'),
                'utcDate' => $sermon->date,
                'realDate' => $sermon->date,
                'language' => $sermon->language,
                'dateLocale' => [
                    'shortMonth' => \Yii::$app->formatter->asDate($sermon->date, 'php:M'),
                    'longMonth' => \Yii::$app->formatter->asDate($sermon->date, 'php:F'),
                ],
                'scripture' => $this->scriptureText($sermon),
                'scriptureData' => $sermon->scriptures,
                'groupCode' => $sermon->group->code,
                'youtube' => $sermon->youtube,
            ];
            $ret[] = $item;
        }
        return [
            'count' => $query->count(),
            'total' => $query->limit(null)->offset(null)->count(),
            'query' => $query->createCommand()->getRawSql(),
            'sermons' => $ret,
        ];
    }
    private function addWebProtocol($files)
    {
        $ret = [];
        foreach ($files as $id => $file) {
            if (startsWith('//', $file)) {
                $ret[] = "https:" . $file;
            } elseif (startsWith('http://', $file)) {
                $ret[] = str_replace("http:", "https:", $file);
            } else {
                $ret[] = $file;
            }
        }
        return $ret;
    }
    private function scriptureText($sermon)
    {
        $ret = "";
        if (empty($sermon->scriptures)) return "";
        foreach ($sermon->scriptures as $scripture) {
            if (!empty($ret)) $ret .= "; ";
            if (isset($scripture['text'])) {
                $ret .= $scripture['text'];
            }
        }
        return $ret;
    }

    /**
     * old url: /api/sermons/insert SermonsInsertR POST
     */
    public function actionCreate()
    {   
        $sermon = new Sermon();
        $sermon->hits = 0;
        $sermon->hidden = 0;
        $post = \Yii::$app->request->post();
        \Yii::warning(VarDumper::dumpAsString($post));
        if (empty($post)) {
            return "no input";
        }
        $random = \Yii::$app->request->post('random', null);
        $hash = \Yii::$app->request->post('hash', null);
        if($random === null) {
            throw new ForbiddenHttpException('bad random');
        }
        if($hash === null) {
            throw new ForbiddenHttpException('bad hash');
        }
        $calcHash = hash_hmac('sha1', $random, \Yii::$app->params['secret']);
        if ($calcHash != $hash) {
            throw new ForbiddenHttpException('wrong hash');
        }

        if (!isset($post['groupCode'])) {
            throw new BadRequestHttpException('No Group Code set');
        }

        $group = Group::findOne(['code' => $post['groupCode']]);
        if (!$group) {
            $group = new Group();
            $group->code = $post['groupCode'];
            $group->name = $post['groupCode'];
            $group->save();
        }

        $sermon->title = $post['title'];
        $sermon->speaker = $post['speaker'];
        $sermon->date = $post['date'];
        $sermon->seriesName = $post['seriesName'];
        $sermon->files = $post['files'];
        $sermon->scriptures = Scripture::parse($post['scripture']);
        $sermon->groupId = $group->id;
        $sermon->language = $post['language'];
        $sermon->duration = intval(\Yii::$app->request->post('duration', 0));
        $sermon->youtube = \Yii::$app->request->post('youtube', null);

        if (!$sermon->save()) {
            throw new BadRequestHttpException(VarDumper::dumpAsString($sermon->errors));
        }
        return ['response' => 'ok'];
    }
    public function actionAdd($id)
    {
        $sermon = $this->findModel($id);
        $post = \Yii::$app->request->post();

        \Yii::$app->datadog->event('Trying to add: ', [
            'text'       => "adding: \n" . VarDumper::dumpAsString($post),
            'alert_type' => 'success',
            'tags' => 'vue-sermons'
        ]);

        if (empty($post)) {
            return "no input";
        }

        $random = \Yii::$app->request->post('random', null);
        $hash = \Yii::$app->request->post('hash', null);
        if($random === null) {
            throw new ForbiddenHttpException('bad random');
        }
        if($hash === null) {
            throw new ForbiddenHttpException('bad hash');
        }
        $calcHash = hash_hmac('sha1', $random, \Yii::$app->params['secret']);
        if ($calcHash != $hash) {
            throw new ForbiddenHttpException('wrong hash');
        }
        
        if (isset($post['files'])) {
            $sermon->files = ArrayHelper::merge($sermon->files, $post['files']);
        }
        if (isset($post['youtube'])) {
            $sermon->youtube = $post['youtube'];
        }

        if ($sermon->save()) {
            return ['response' => 'changed ', 'files' => $sermon->files];
        } else if (!empty($sermon->errors)) {
            throw new BadRequestHttpException(VarDumper::dumpAsString($sermon->errors));
        } else {
            \Yii::$app->response->setStatusCode(304);
            return ['response' => 'not changed', 'files' => $sermon->files];
        }
    }


    public function actionSeries($group, $language = "")
    {
        $sermons = Sermon::find()->joinWith('group');
        if (isset($group) && !empty($group)) {
            $sermons = $sermons->andWhere(['group.code' => $group]);
        }
        if (!empty($language)) {
            $sermons = $sermons->andWhere(['language' => $language]);
        }
        $ret = [];
        foreach ($sermons->select('seriesName')->distinct()->orderBy(["seriesName" => SORT_ASC])->each() as $item) {
            if (!empty($item->seriesName)) {
                $ret[] = $item->seriesName;
            }
        }
        return $ret;
    }
    public function actionSeriesData()
    {
        $filter = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : [];
        $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : [];
        $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 0;
        $offset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;

        $sermons = Sermon::find();
        if (isset($filter['groupCode']) && !empty($filter['groupCode'])) {
            $sermons = $sermons->joinWith('group')->andWhere(['group.code' => $filter['groupCode']]);
        }
        $ret = [];

        foreach ($sermons->select('seriesName')->distinct()->orderBy(["date" => SORT_DESC])->each() as $item) {
            /** @var Sermon $item */
            if (!empty($item->seriesName)) {
                $allSermons = Sermon::find()->where(['seriesName' => $item->seriesName]);
                $image = \Yii::$app->params['defaultCover'];
                $groupCode = '';

                foreach ($allSermons->each() as $sermon) {
                    if (!empty($sermon->files['image'])) {
                        $image = $sermon->files['image'][0];
                    }
                    $groupCode = $sermon->group ? $sermon->group->code : '';
                }
                $series =  [
                    'name' => $item->seriesName,
                    'image' => $image,
                    'count' => $allSermons->count(),
                    'groupCode' => $groupCode
                ];

                $ret[] = $series;
            }
            if (isset($limit) && !empty($limit)) {
                if (count($ret) >= $limit)
                    break;
            }
        }
        return $ret;
    }

    public function actionSpeakers($group, $language = "", $minCount = 2)
    {
        $sermons = Sermon::find();
        if (isset($group) && !empty($group)) {
            $sermons = $sermons->joinWith('group')->where(['group.code' => $group]);
        }
        //

        $sermons = $sermons
            ->select(['COUNT(sermon.id) AS count', 'speaker'])
            ->groupBy('speaker')
            ->orderBy(["speaker" => SORT_ASC]);
        if (!empty($language)) {
            $sermons->andFilterWhere(['language' => $language]);
        }
        $sermons = $sermons->createCommand()->queryAll();

        $ret = [];

        foreach ($sermons as $item) {
            if (!empty($item['speaker']) && $item['count'] > $minCount) {
                $ret[] = $item['speaker'];
            }
        }
        return $ret;
    }
    public function actionLanguages($group)
    {
        $sermons = Sermon::find();
        if (isset($group) && !empty($group)) {
            $sermons = $sermons->joinWith('group')->where(['group.code' => $group]);
        }
        $sermons = $sermons->select(['language',])->distinct()->orderBy(["language" => SORT_ASC]);
        $ret = [];
        foreach ($sermons->each() as $item) {
            if (!empty($item->language)) {
                $ret[] = $item->language;
            }
        }
        return $ret;
    }

    public function actionInfo()
    {
        return [
            'cover' => \Yii::$app->params['defaultCover'],
            'name' => \Yii::$app->params['channelName'],
            'subtitle' => \Yii::$app->params['channelSubtitle'],
            'description' => \Yii::$app->params['channelDescription'],
        ];
    }


    /**
     * @param $id
     * @return Sermon
     */
    protected function findModel($id)
    {
        $sermon = Sermon::findOne($id);
        if ($sermon === null) {
            throw new NotFoundHttpException('The requested sermon does not exist.');
        }
        return $sermon;
    }
}
