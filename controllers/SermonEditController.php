<?php
namespace app\controllers;
use app\models\Sermon;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class SermonEditController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['list', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                
                ],
            ],

        ];
    }


    public function actionList()
    {
        $searchModel = new \app\models\SermonSearch();
        $sermons = $searchModel->search(\Yii::$app->request->get());
        return $this->render('list', ['sermons' => $sermons, 'filter' => $searchModel]);
    }

    public function actionUpdate($id)
    {
        $sermon = $this->findModel($id);
        if ($sermon->load(\Yii::$app->request->post())) {
            $sermon->files = array_map(function ($item) {return array_filter($item);}, $sermon->files);
            if(!$sermon->save()) {
                \Yii::error($sermon->errors);
            }
            if($_POST['type'] == 'saveAndClose') {
                $this->redirect(['list']);
            }
        }
        return $this->render('update', ['sermon' => $sermon]);
    }

    /**
     * @param int $id
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $this->redirect(['list']);
    }



    /**
     * @param int $id
     * @return Sermon
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $sermon = Sermon::findOne($id);
        if ($sermon === null) {
            throw new NotFoundHttpException('The requested sermon does not exist.');
        }
        return $sermon;
    }
}
