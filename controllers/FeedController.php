<?php
namespace app\controllers;

use app\helpers\Feed;
use app\models\Group;
use app\models\Sermon;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FeedController extends JsonController
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/xml' => Response::FORMAT_RAW,
                    'application/rss+xml' => Response::FORMAT_RAW,
                    'text/xml' => Response::FORMAT_RAW,
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws NotFoundHttpException, \Exception
     */
    public function actionFeed($groupCode="", $language="")
    {
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');

        $sermons = Sermon::find()->joinWith('group')
                         ->andWhere(['hidden' => false])
                         ->andWhere(['<=', 'date', $today])
                         ->limit(1000);
        $groupName = "";
        $sermons = $sermons->andWhere(['or', ['<>', 'showOnlyDirect', 1], ['showOnlyDirect' => NULL]]);

        echo $sermon->createCommand()->getRawSql();
        die();


        if(!empty($groupCode)) {
            $group = Group::findOne(['code' => $groupCode]);
            if(!$group) {
                throw new NotFoundHttpException("group not found");
            }
            $groupName = $group->name;
            $groupCode = $group->code;
            $sermons = $sermons->andWhere(['group.code' => $groupCode]);
        }
        if(!empty($language)) {
            $language = substr($language,0,2);
            $sermons = $sermons->andWhere(['sermon.language' => $language]);
        }
        return Feed::createRssFeed($this, $sermons, $groupName, $groupCode, $language);
    }


    /**
     * @return string
     * @throws NotFoundHttpException, \Exception
     */
    public function actionRss()
    {
        $sermons = $this->getQuery()->limit(1000);
        $groupName = "";
        $language = "";

        $groupCode = "";
        if(isset($_REQUEST['filter']) && isset($_REQUEST['filter']['groupCode']) && !empty($_REQUEST['filter']['groupCode'])) {
            $group = Group::findOne(['code' => $_REQUEST['filter']['groupCode']]);
            if(!$group) {
                throw new NotFoundHttpException("group not found");
            }
            $groupName = $group->name;
            $groupCode = $group->code;
        }
        if(isset($_REQUEST['filter']) && isset($_REQUEST['filter']['language'])) {
            $language = substr($_REQUEST['filter']['language'],0,2);
        }
        return Feed::createRssFeed($this, $sermons, $groupName, $groupCode, $language);
    }

    /**
     * @return ActiveQuery
     * @throws \Exception
     */
    private function getQuery()
    {
        $filter = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : [];
        $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : [];
        $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 0;
        $offset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');

        $sermons = Sermon::find()->joinWith('group')
                         ->andWhere(['hidden' => false])
                         ->andWhere(['<=', 'date', $today]);
        $sermons = $sermons->andWhere(['or', ['<>', 'showOnlyDirect', 1], ['showOnlyDirect' => NULL]]);


        if(isset($filter['groupCode']) && !empty($filter['groupCode'])) {
            $sermons = $sermons->andWhere(['group.code' => $filter['groupCode']]);
        }
        if(isset($filter['seriesName']) && !empty($filter['seriesName'])) {
            $sermons = $sermons->andWhere(['like', 'sermon.seriesName', $filter['seriesName']]);
        }
        if(isset($filter['title']) && !empty($filter['title'])) {
            $sermons = $sermons->andWhere(['like', 'sermon.title', $filter['title']]);
        }
        if(isset($filter['speaker']) && !empty($filter['speaker'])) {
            $sermons = $sermons->andWhere(['like', 'sermon.speaker', $filter['speaker']]);
        }
        if(isset($filter['id'])) {
            $sermons = $sermons->andWhere(['sermon.id' => $filter['id']]);
        }
        if(isset($filter['language']) && !empty($filter['language'])) {
            $sermons = $sermons->andWhere(['sermon.language' => $filter['language']]);
        }
        if(isset($filter['all'])) {
            $sermons = $sermons->andFilterWhere(['like', 'seriesName', $filter['all']]);
            $sermons = $sermons->orFilterWhere(['like', 'title', $filter['all']]);
            $sermons = $sermons->orFilterWhere(['like', 'speaker', $filter['all']]);
        }
        if($limit != 0) {
            $sermons = $sermons->limit($limit);
        }

        if($offset != 0)  {
            $sermons = $sermons->offset($offset);
        }

        if(!empty($sort)) {
            $s = [];
            foreach($sort as $key => $order) {
                if($order === "asc") {
                    $s[$key] = SORT_ASC;
                } else {
                    $s[$key] = SORT_DESC;
                }
            }
            $sermons = $sermons->orderBy($s);
        } else {
            $sermons = $sermons->orderBy(['date' => SORT_DESC]);
        }
        return $sermons;
    }


    public function actionInfo()
    {
        return [
            'cover' => \Yii::$app->params['defaultCover'],
            'name' => \Yii::$app->params['channelName'],
            'subtitle' => \Yii::$app->params['channelSubtitle'],
            'description' => \Yii::$app->params['channelDescription'],
        ];
    }
}
