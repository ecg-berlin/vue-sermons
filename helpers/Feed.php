<?php

namespace app\helpers;

class Feed
{
    public static function createFeedUrl($url)
    {
        if (startsWith('//', $url)) {
             return "http:" . $url;
        } elseif (startsWith('https://', $url)) {
            return  str_replace("https:", "http:", $url);
        } else {
            return $url;
        }
    }
    public static function createRssFeed($controller, $query, $groupName, $groupCode, $language="") 
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = \Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml; charset=utf-8');
        $params = \Yii::$app->params;
        $path = 'https:'.$params['covers'] . '/';
        if(!empty($groupCode)) {
            $path .= $groupCode;
            if(!empty($language)) {
                $path .= '-'.$language;
            }
            $path .= '.jpg';
        } else {
            $path .= "all.jpg";
        }

        $title = $params['channelName'];
        if (!empty($groupName)) {
            $title .= " - $groupName";
        }
        if (!empty($language)) {
            $title .= " ($language)";
        } 
        
        $subtitle = $params['channelSubtitle'];
        $description = $params['channelDescription'];
        $link = $params['channelLink'];

        if(!empty($groupCode) && isset($params['groups']) && isset($params['groups'][$groupCode])) {
            $groups = $params['groups'][$groupCode];
            $config = null;

            if(isset($groups[$language])) {
                $config = $groups[$language];
            } else if(count($groups) == 1) {
                $config = array_pop($groups);
            } 
            if($config) {
                if(isset($config['title'])) {
                    $title = $config['title'];
                }
            }

        }

        //the hidden sermons are not included here 
        $lastSermon = $query->orderBy(['date' => SORT_DESC])->one();
        if (!$lastSermon) {
            $lastBuildDate = new \DateTime("2017-01-01");
        } else {
            $lastBuildDate = new \DateTime($lastSermon->date);
        }

        return $controller->renderPartial('/sermon/rss', [
            'title' => 'asd',
            'sermons' => $query,
            'groupName' => $groupName,
            'groupCode' => $groupCode,
            'language' => $language,
            'title' => $title,
            'subtitle' => $subtitle,
            'description' => $description,
            'link' => $link,
            'lastBuildDate' => $lastBuildDate,
            'cover' => $path
        ]);
    }

    

}