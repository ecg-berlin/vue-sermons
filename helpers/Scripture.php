<?php

namespace app\helpers;


class Scripture
{
    private $booksDe =
        [['1.Mose', '1.Mo', '1Mo',],
            ['2.Mose', '2.Mo', '2Mo',],
            ['3.Mose', '3.Mo', '3Mo',],
            ['4.Mose', '4.Mo', '4Mo',],
            ['5.Mose', '5.Mo', '5Mo',],
            ['Josua', 'Jos',],
            ['Richter', 'Ri',],
            ['Rut', 'Ru',],
            ['1.Samuel', '1Sam',],
            ['2.Samuel', '2Sam',],
            ['1.Könige', '1Kön',],
            ['2.Könige', '2Kön',],
            ['1.Chronik', '1Chr',],
            ['2.Chronik', '2Chr',],
            ['Esra', 'Esr'],
            ['Nehemia', 'Ne', 'Neh'],
            ['Ester', 'Es'],
            ['Hiob', 'Hi'],
            ['Psalm', 'Ps',],
            ['Sprüche', 'Spr',],
            ['Prediger', 'Pred',],
            ['Hohelied', 'Hoh'],
            ['Jesaja', 'Jes',],
            ['Jeremia', 'Jer',],
            ['Klagelieder', 'Kla',],
            ['Hesekiel', 'Hes',],
            ['Daniel', 'Dan',],
            ['Hosea', 'Hos'],
            ['Joel', 'Joe'],
            ['Amos', 'Am'],
            ['Obadja', 'Ob'],
            ['Jona', 'Jon'],
            ['Micha', 'Mic'],
            ['Nahum', 'Na'],
            ['Habakuk', 'Hab'],
            ['Zefanja', 'Zef'],
            ['Haggai', 'Hag'],
            ['Sacharja', 'Sac'],
            ['Maleachi', 'Mal',],
            ['Matthäus', 'Mt',],
            ['Markus', 'Mk',],
            ['Lukas', 'Lk',],
            ['Johannes', 'Joh',],
            ['Apostelgeschichte', 'Apg',],
            ['Römer', 'Röm',],
            ['1.Korinther', '1.Kor', '1Kor',],
            ['2.Korinther', '2.Kor', '2Kor',],
            ['Galater', 'Gal',],
            ['Epheser', 'Eph',],
            ['Philipper', 'Phil',],
            ['Kolosser', 'Kol',],
            ['1.Thessalonicher', '1Th', '1Thess'],
            ['2.Thessalonicher', '2Th', '2Thess'],
            ['1.Timotheus', '1.Tim', '1Tim',],
            ['2.Timotheus', '2.Tim', '2Tim',],
            ['Titus', 'Tit',],
            ['Philemon', 'Phm'],
            ['Hebräer', 'Heb',],
            ['Jakobus', 'Jak',],
            ['1.Petrus', '1.Petr', '1Petr',],
            ['2.Petrus', '2.Petr', '2Petr',],
            ['1.Johannes', '1.Joh', '1Joh',],
            ['2.Johannes', '2.Joh', '2Joh',],
            ['3.Johannes', '3.Joh', '3Joh',],
            ['Judas', 'Jud',],
            ['Offenbarung', 'Offb',]];

    private $regex_type4 = "";
    private $regex_type3 = "";
    private $regex_type2 = "";
    private $regex_type1 = "";
    private $regex_type0 = "";

    public static function parse($reference, $language='de')
    {
        return 
        [
            [
                'text' => $reference
            ]
        ];
    }

}