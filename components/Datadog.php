<?php

namespace app\components;

use yii\base\Component;

use DataDog\DogStatsd;

class Datadog extends Component
{
    private $interface = null;
    public $apiKey;
    public $appKey;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $conf = ['api_key' => $config['apiKey']];
        $this->interface = new DogStatsd($conf);
    }

    public function increment($name, $value, $tags = [])
    {
        $defaultTags = ['tagname' => 'bko-' . \Yii::$app->params['code']];
        $tags = array_merge($tags, $defaultTags);
        $this->interface->increment($name, $value, $tags);
    }
    public function timing($name, $value, $tags = [])
    {
        $defaultTags = ['tagname' => 'bko-' . \Yii::$app->params['code']];
        $tags = array_merge($tags, $defaultTags);
        $this->interface->timing($name, $value, $tags);
    }

    public function histogram($name, $value, $tags = [])
    {
        $defaultTags = ['tagname' => 'bko-' . \Yii::$app->params['code']];
        $tags = array_merge($tags, $defaultTags);
        $this->interface->histogram($name, $value, $tags);
    }

    public function event($name, $value)
    {
        $this->interface->event($name, $value);
    }

    public function init()
    {
        parent::init();
    }
}
