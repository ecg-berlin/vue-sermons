<?php

namespace app\components;

use app\parts\Access;
use Yii;

use yii\log\Logger;
use yii\log\Target;

/**
 * Datadog Log Target
 * Send errors and warnings count to Datadog
 *
 * Class DatadogTarget
 * @package app\components
 */
class DatadogTarget extends Target
{

    /**
     * Defaults levels to log
     * @var array
     */
    public $levels = ['error'];
    /**
     * Prefix for metric name (required)
     * @var string
     */
    public $metricPrefix;

    /**
     * Tags, merged with message category (optional)
     * @var array
     */
    public $tags = [];
    public function init()
    {
        parent::init();
    }
    /**
     * Ignored categories. Defaults to ['yii\web\HttpException:404']
     * @var array
     */
    public $ignoredCategories = [];

    /**
     * {@inheritdoc}
     */
    public function export()
    {
        foreach ($this->messages as $message) {
            if (in_array($message[2], $this->ignoredCategories, true)) {
                continue;
            }
            if (!in_array(Logger::getLevelName($message[1]), $this->levels, true)) {
                continue;
            }
            $level = $message[1];
            $alertType = 'success';
            if ($level === Logger::LEVEL_ERROR) {
                $alertType = 'error';
            } elseif ($level === Logger::LEVEL_WARNING) {
                $alertType = 'warning';
            } elseif ($level === Logger::LEVEL_INFO) {
                $alertType = 'info';
            }


            $title = '';

            $text = '' . $message[0] . "\n\n";
            if (count($message[4]) > 0) {
                $more = $message[4];
                foreach ($more as $m) {
                    if (isset($m['file'])) {
                        $text .= $m['file'] . ':' . $m['line'] . "\n\n";
                    }
                    if (isset($m['trace'])) {
                        $text .= implode("\n", $m['trace']);
                    }
                }
            }
            if (isset($_SERVER['REQUEST_URI'])) {
                $text .= 'Url: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "\n";
            }
            if (isset($_SERVER['HTTP_REFERER'])) {
                $text .= 'Referer: ' . $_SERVER['HTTP_REFERER'] . "\n";
            }

            \Yii::$app->datadog->event($title, [
                'text'       => $text,
                'alert_type' => $alertType,
                'tags' => array_merge($this->tags, [$message[2]])
            ]);
        }
    }
}
