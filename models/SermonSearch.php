<?php

namespace app\models;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SermonSearch extends Sermon
{
    public function rules()
    {
        // only fields in rules() are searchable
        return [
            [['title', 'speaker', 'date', 'seriesName', 'hidden'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }
    public function search($params)
    {
        $query = Sermon::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['date' => SORT_DESC]]
        ]);
        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'speaker', $this->speaker]);
        $query->andFilterWhere(['like', 'date', $this->date]);
        $query->andFilterWhere(['like', 'seriesName', $this->seriesName]);
        $query->andFilterWhere(['hidden' => $this->hidden]);

        return $dataProvider;
    }
}
