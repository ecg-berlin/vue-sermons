<?php

namespace app\models;

/**
 * Class Sermon
 * @package app\models
 *
 * @property integer $id
 * @property string $title
 * @property integer $groupId
 * @property array $language
 * @property string $picture
 * @property string $notes
 * @property string $filesJson
 * @property array $files
 * @property array $scriptures
 * @property string $scripturesJson
 * @property string $date
 * @property integer $hits
 * @property string $seriesName
 * @property string $speaker
 * @property string $youtube
 * @property integer hidden
 * @property integer $duration (in seconds)
 * @property boolean $showOnlyDirect
 *
 * @property Group $group
 *
 */

class Sermon extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'sermon';
    }
    public function rules()
    {
        return [
            [['title', 'speaker', 'groupId', 'language'], 'required'],
            [['title',  'filesJson', 'scripturesJson', 'picture', 'notes', 'speaker', 'seriesName', 'youtube'], 'string'],
            [['hits', 'groupId', 'hidden', 'duration', 'showOnlyDirect'], 'integer'],
            [['files', 'scriptures'], 'safe']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::class, ['id' => 'groupId']);
    }

    public function getFiles()
    {
        $data = json_decode($this->filesJson, true);
        if ($data === false) {
            return [];
        }
        return $data;
    }
    public function setFiles($newValue)
    {
        if (isset($newValue['other'])) {
            $newValue['other'] = array_unique($newValue['other']);
        }
        if (isset($newValue['audio'])) {
            $newValue['audio'] = array_unique($newValue['audio']);
        }
        if (isset($newValue['video'])) {
            $newValue['video'] = array_unique($newValue['video']);
        }
        if (isset($newValue['image'])) {
            $newValue['image'] = array_unique($newValue['image']);
        }
        $this->filesJson = json_encode($newValue);
    }
    public function getScriptures()
    {
        $data = json_decode($this->scripturesJson, true);
        if ($data === false) {
            return [];
        }
        return $data;
    }

    public function setScriptures($response)
    {
        $this->scripturesJson = json_encode($response);
    }

    public function getAlternativeCover()
    {
        if (isset(\Yii::$app->params['covers'])) {
            $path = \Yii::$app->params['covers'];
            return "$path/{$this->group->code}-{$this->language}.jpg";
        }
        return "";
    }
}
