<?php

namespace app\models;

/**
 * Class Group
 * @package app\models
 * @property string $name
 * @property string $code
 * @property string $titlesJson
 */
class Group extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'group';
    }

    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
        ];
    }

    public function getTitles()
    {
        $data = json_decode($this->titlesJson, true);
        if ($data === false) {
            return [];
        }
        return $data;
    }
    public function setTitles($response)
    {
        $this->titlesJson = json_encode($response);
    }
}
