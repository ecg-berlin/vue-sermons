<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends \yii\base\BaseObject implements IdentityInterface
{
    public $username;
    public $passwordHash;

     /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (\Yii::$app->params['users'] as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }
        return null;
    }
  
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->passwordHash === crypt($password, '$6$'.\Yii::$app->params['secret'].'$');
    }

    public static function findIdentity($id)
    {
        foreach (\Yii::$app->params['users'] as $user) {
            if (strcasecmp($user['username'], $id) === 0) {
                return new static($user);
            }
        }
    }
    public function getId()
    {
        return $this->username;
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }
    public function getAuthKey()
    {
        return false;
    }
    public function validateAuthKey($authKey)
    {
        return false;
    }
    

   
}
