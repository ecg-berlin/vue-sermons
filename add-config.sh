#!/bin/bash
set -e
DECODED=$(echo $CONFIG_REPO_PATH | base64 -d)
git clone ${DECODED:-git@gitlab.com:ecg-berlin/vue-sermons-config.git} tmp-config
cp -rf tmp-config/ansible ./
chmod 600 ansible/data/id_rsa
chmod 600 ansible/data/id_rsa.pub
rm ./tmp-config/ -rf