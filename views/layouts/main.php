<?php
use yii\helpers\Html;
use app\assets\AppAsset;
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?= Yii::getAlias('@web') ?>"/>

    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    

    <link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/stylesheets/default/style.css">
    <script src="<?= Yii::getAlias('@web') ?>/vue/dist/sermons.umd.min.js"></script>
    <script>
        var SERMONS_HOST = '<?= aurl(['/']);?>';
        var SERMONS_STATIC_SITE = '<?= aurl(['sermon/index']);?>';
    </script>
    <link rel="alternate" type="application/rss+xml" title="Podcast" href="<?= aurl(['feed/'])?>"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(\Yii::$app->name) ?> - <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>
    <div class="wrapper">
        <main class="main-container">
            <div class="container">
                <?= $content; ?>
            </div>
        </main>
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
