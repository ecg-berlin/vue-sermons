<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\Form;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->registerCss("
body,html {
    background: #fafafa; 
    background-image: url(".Yii::getAlias('@web')."/images/blurred-background.jpg);
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    height: 100%;
}");
?>

<div class="wrapper-page login">
        <div class="text-center">
            <h2>
                Login
            </h2>
        </div>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal'],
        ]);
        ?>

        <?= $form->field($model, 'username')
            ->textInput(['autofocus' => true])
            ->input('text', ['placeholder' => t('site.login.username')])
            ->label(false) ?>

        <?= $form->field($model, 'password')
            ->passwordInput()
            ->input('password', ['placeholder' => t('site.login.password')])
            ->label(false) ?>

        <div class="form-group text-right">

                <?= Html::submitButton(t('site.login.login'),
                    ['class' => 'btn btn-primary btn-custom w-md', 'name' => 'login-button']) ?>
          
        </div>

    <?php ActiveForm::end(); ?>
</div>