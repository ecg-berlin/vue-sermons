<?php
/** @var \yii\db\ActiveQuery $sermons */
use app\helpers\Feed;
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
$url = aurl(['sermon/feed', 'groupCode' => $groupCode, 'language' => $language]);
?>

<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:googleplay="http://www.google.com/schemas/play-podcasts/1.0/play-podcasts.xsd">
    <channel>
        <title><?= $title ?></title>
        <atom:link href="<?= str_replace("&", "&amp;", aurl($url)); ?>" rel="self" type="application/rss+xml" />
        <link><?= $link ?></link>
        <description><?= $description ?></description>
        <image>
            <title><?= $title ?></title>
            <url><?= $cover ?></url>
            <link><?= $link ?></link>
        </image>

        <lastBuildDate><?= $lastBuildDate->format(DateTime::RFC2822) ?></lastBuildDate>
        <language><?= $language ?></language>
        <sy:updatePeriod>hourly</sy:updatePeriod>
        <sy:updateFrequency>3</sy:updateFrequency>
        <generator>https://gitlab.com/ecg-berlin/vue-sermons</generator>
        <copyright><?= Yii::$app->params['copyright'] ?></copyright>
        <itunes:subtitle><?= $subtitle ?></itunes:subtitle>
        <itunes:summary>
            <?= $description ?></itunes:summary>
        <itunes:explicit>clean</itunes:explicit>
        <itunes:author><?= Yii::$app->params['channelName'] ?></itunes:author>
        <itunes:owner>
            <itunes:name><?= Yii::$app->params['channelName'] ?></itunes:name>
            <itunes:email><?= Yii::$app->params['channelEMail'] ?></itunes:email>
        </itunes:owner>
        <itunes:category text="Religion &amp; Spirituality">
            <itunes:category text="Christianity" />
        </itunes:category>
     
        <itunes:image href="<?= $cover ?>"></itunes:image>
        <itunes:link rel="image" type="video/jpeg" ><?= $cover ?></itunes:link>

        <googleplay:email><?= Yii::$app->params['channelEMail'] ?></googleplay:email>
        <googleplay:description><?= $description ?></googleplay:description>
        <googleplay:category text="Religion &amp; Spirituality" />
        <googleplay:image href="<?= $cover ?>" />

        <?php foreach ($sermons->each() as $sermon) {
            /** @var \app\models\Sermon $sermon */
            $date = new \DateTime($sermon->date);
            if (!isset($sermon->files["audio"])) {
                continue;
            }
            if (empty($sermon->files["audio"])) {
                continue;
            }

            $mp3 = 'http:' . $sermon->files["audio"][0];
            $mp3 = str_replace(" ", rawurlencode(" "), $mp3);

            $mp3 = str_replace("&", "&amp;", $mp3);
            $mp3 = str_replace("<", "&lt;", $mp3);
            $mp3 = str_replace("\"", "&quot;", $mp3);
            $mp3 = str_replace("'", "&apos;", $mp3);


            $title  = htmlspecialchars($sermon->title, ENT_XML1, 'UTF-8');
            $author = htmlspecialchars($sermon->speaker, ENT_XML1, 'UTF-8');
            $permaLink = aurl(['sermon/index', 'id' => $sermon->id]);
            echo "  <item>\n";
            echo "    <link>" . $permaLink . "</link>\n";
            echo "    <title>$title ($author)</title>\n";
            echo "    <pubDate>{$date->format(DateTime::RFC2822)}</pubDate>\n";
            echo "    <guid isPermaLink='true'>" . $permaLink . "</guid>\n";
            echo "    <itunes:author>{$author}</itunes:author>\n";
            echo "    <itunes:explicit>clean</itunes:explicit>\n";
            echo "    <itunes:episodeType>full</itunes:episodeType>\n";
            $images = $sermon->files['image'];
            if(!empty($images)) {
                $image = Feed::createFeedUrl(array_shift($images));
                echo "    <itunes:image href='$image'/>\n";
            }
            


            if(!empty($sermon->duration) && $sermon->duration > 0) {
                echo "    <itunes:duration>".gmdate("H:i:s", $sermon->duration)."</itunes:duration>\n";
            }
            $desc = "";
            $desc .= "\"$title\" von $author am " . \Yii::$app->formatter->asDate($date->format(DateTime::RFC2822), "medium") . ". \n\r";

            $desc .= "Weitere Infos unter $permaLink\n";

            if (!empty($desc)) {
                echo "    <description>$desc</description>\n";
                echo "    <itunes:summary>$desc</itunes:summary>\n";
            }

            echo "    <enclosure length='1' url='$mp3' type='audio/mpeg'/>\n";
            echo "  </item>";
        } ?>
    </channel>
</rss>