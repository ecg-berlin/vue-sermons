<?php
/** @var \app\models\Sermon $sermon */
?>
<h1><?= $sermon->title?></h1>
<p>Sprecher: <?= $sermon->speaker?></p>
<p>Datum: <?= $sermon->date?></p>
<p>Serie: <?= $sermon->seriesName?></p>
<?php foreach ($sermon->files as $type => $files) {
  if(count($files) != 0) {
    echo "<p>";
    foreach ($files as $file) {
      echo "<a href='$file'>$file</a>";
    }
    echo "</p>";
  }
}?>

<single-sermon :sermon-id='<?= $sermon->id?>'></single-sermon>