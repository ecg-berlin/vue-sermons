<?php

use yii\helpers\Html;

/** @var \yii\bootstrap\ActiveForm $form */
/** @var \app\models\Sermon $sermon */
?>

<?php $form = \yii\bootstrap\ActiveForm::begin(); ?>

<div class="card-box">
  <div class="row">
    <div class="col-md-12 col-lg-8">
      <h1><?= $sermon->title ?></h1>
    </div>
    <div class="col-md-12 col-lg-4">
      <br />
      <div class="pull-right">
        <a class="btn btn-danger" href="<?= url( ['sermon-edit/list'] ) ?>">
          Zurück
        </a>
        <button name="type" type="submit" class="btn btn-primary" value="saveAndClose">
          Speichern und Schließen
        </button>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
        <?= $form->field( $sermon, 'title' )->textInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field( $sermon, 'hidden' )->checkbox() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field( $sermon, 'showOnlyDirect' )->checkbox() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field( $sermon, 'seriesName' )->textInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field( $sermon, 'speaker' )->textInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field( $sermon, 'hits' )->textInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field( $sermon, 'youtube' )->textInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field( $sermon, 'duration' )->textInput() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field( $sermon, 'scripturesJson' )->textarea() ?>
    </div>
    <div class="col-md-12">
        <?= $form->field( $sermon, 'language' )->textInput() ?>
    </div>
  </div>
</div>
<div class="card-box">
  <div class="row">
    <div class='col-md-12'><h2>Dateien</h2></div>
      <?php foreach($sermon->files as $groupName => $files) {
          echo "<div class='col-md-12'>";
          echo "<h3>" . $groupName . "</h3>";
          foreach($files as $file) {
              $file = \yii\helpers\Html::encode( $file );
              echo "<input type='text'  class='form-control' name='Sermon[files][$groupName][]' value='$file'>";
              echo "<br />";
          }

          echo "<input type='text'  class='form-control' name='Sermon[files][$groupName][]' value='' placeholder='Neue Datei hinzufügen'>";

          echo "</div>";
      }
      ?>
  </div>
</div>
<br/>
<div class="row">
  <div class="col-xs-12">
    <div class="pull-right">
      <a class="btn btn-danger" href="<?= url( ['sermon-edit/list'] ) ?>">
        Schließen
      </a>
      <button name="type" type="submit" class="btn btn-default" value="save">
        Speichern
      </button>
      <button name="type" type="submit" class="btn btn-primary" value="saveAndClose">
        Speichern und Schließen
      </button>
    </div>
  </div>
</div>
<br/>
<br/>
<?php \yii\bootstrap\ActiveForm::end(); ?>
