<?php
/** @var \app\models\SermonSearch $filter */
/** @var \yii\data\ActiveDataProvider $sermons */

?>
<div class="card-box">

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $sermons,
        'filterModel' => $filter,
        'columns' => [
            'title',
            'speaker',
            'date',
            'hidden',
            'seriesName',
            'hits',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                ]
            ]
        ],
    ]) ?>

</div>
