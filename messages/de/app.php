<?php
return [
    'ID' =>' Kennung',
    'site.login.username' => 'Benutzername',
    'site.login.password' => 'Password',
    'site.login.rememberMe' => 'Langfristig einloggen',
    'site.login.login' => 'Login'
];