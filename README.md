# Vue-Sermons

Vue-Sermons is a simple web-plattform to show, search, serve and play audio and video sermon recordings.

![Logo](web/images/banner.png "Logo")

## 1. Features

+ possible to include in every website (wordpress, joomla etc…)
+ self-hosted - you've got full control over your data
+ supports:
  + multiple sermon languages
  + self hosted video/self or hosted audio/youtube/vimeo
  + multiple groups (eg sermons, sunday school, youth sermons)
  + multiple series (eg. preaching series)
  + speakers
  + bible references
  + documents as attachments
+ sharing
+ search by speaker, date, series and bible reference
+ optimized podcast feed

## 2. How its working

This software displays, serves, plays, searches and edits sermons, which are already in a database.

You can also use [vue-sermon-uploader](https://gitlab.com/ecg-berlin/vue-sermons-uploader) for an easy upload via ftp. Just store your file on your FTP server and the script will take care to insert the sermon in Vue-Sermons.

The backend is build with PHP 8, Yii2 and Mariadb - the frontend uses vue 2.

To include Vue-Sermons on any site you need only to include a js and css files and add some html tags.

## 3. Deployment / Installation on a web/server

How to install vue-sermons on my webserver?

### 3.1 Via Gitlab CI/CD

Internally we are using the Gitlab CI/CD.

1. Create ssh keys
    + `mkdir ansible/data`
    + `ssh-keygen -t rsa -b 4096 -C "deployment-sermons@example.com" -f ansible/data/id_rsa`
    + `ssh-copy-id -i ansible/data/id_rsa {{username}}@{{host}}` (repeat for every host, see `ansible/hosts.yml`)
2. Create deploy token
    + `echo "https://{{username}:{{password}}@gitlab.com/{{project}}/{{repo}}.git" | base64 -w 1000`
    + Go to the settings of the project itself set CONFIG_REPO_PATH to this output


### 3.2 Download and extract

## 4. Development

How to setup vue sermons for development:

### 4.1 Installation

1. Redis is needed. Installation on [ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04)
2. [Composer](https://getcomposer.org/doc/00-intro.md#installation-nix).
3. [Yarn](https://classic.yarnpkg.com/en/docs/install/]

You can then install this project using the following command:

```bash
php composer.phar install
mkdir web/assets
# make it writable
```

Now you should be able to access the application at: `http://localhost/sermons/web/`

Note: mod_headers is required (`a2enmod headers`)

### 4.2 Running vue

The frontend part is written in vue. You can start it via:

```bash
cd web/vue
# install packages
yarn
# run dev server
yarn dev
```


## 5. API

### Auflisten

URL: `https://predigten.ecg.berlin/sermon/list`
Beispiel:
`https://predigten.ecg.berlin/sermon/list?filter[groupCode]=predigt&filter[speaker]=Theo&sort[hits]=desc&limit=10`

Filter:

```json
filter[groupCode] :: string | string[]
filter[title] :: string
filter[date] :: string
filter[speaker] :: string | string []
filter[seriesName] :: string
filter[language] :: string

filter[id] :: integer | integer[]
```

Sort:

```json
sort[date] = "asc" | "desc"
sort[speaker] = "asc" | "desc"
sort[hits] = "asc" | "desc"
sort[title] = "asc" | "desc"
```

Limit:

```json
(limit = integer)
```

Offset:

```json
(offset = integer)
```

Diese Parameter werden entweder per GET oder POST body angenommen.

Ausgabe:

```json
[
    {
        "id": integer,
        "title": string
        "audio" : string[] (absolute url)
        "video" : string[] (absolute url)
        "other" : string[] (absolute url)
        "speaker": string
        "seriesName": string
        "date" :string (formatted)
        "realDate" : string (ISO 8601 date)
        "scripture": string
        "groupCode": string
        "language": string

    }
]
```

### Atom Feed

URL: `/sermon/rss`

### Einfügen

`/sermon/create`

### Statistic

`/sermon/hit/{id}`


## 6. Integrations

How to integrate Vue-Sermons in a website? You need to include 2 files and add some elements where to display the sermons.

Vue-Sermons also depends on jQuery, Bootstrap 3 and Fontawesome.

### HTML/CSS/JS

The easiest way to include is:

```html
<sermon-container static="https://example.com/static" theme="default">
    <sermon-list></sermon-list>
</sermon-container>
<script src='https://example.com/vue/dist/sermons.umd.min.js'></script>
```

Or this way:

```html
<script>
    var SERMONS_STATIC_SITE = 'https://example.com/static';
</script>
<sermon-list></sermon-list>
<script src='https://example.com/vue/dist/sermons.umd.min.js'></script>
<link rel='stylesheet' href='https://example.com/stylesheets/default/style.css'>
```

### Wordpress

An example for an short for wordpress

```php
<?php
function sermon_list_html($atts)
{
    $group = "";
    $style = "";
    $series = "";
    $url = "";
    $language = "";
    $static = "";

    // Params extraction
    extract(
        shortcode_atts(
            [
                'group'   => '',
                'style' => 'default', //style name
                'series' => '',
                'url' => 'predigten.ecg.berlin', //host
                'language' => 'de', //default style
                'static' => 'https://ecg.berlin/medien/predigt', //site with a single-sermon-widget
            ],
            $atts
        )
    );
    $html = "<script>var SERMONS_STATIC_SITE = '$static';</script>";
    $html .= "<sermon-list in-group='$group' in-series='$series' language='$language'></sermon-list>";
    $html .= "<script src='//$url/vue/dist/sermons.umd.min.js'></script>";
    $html .= "<link rel='stylesheet' href='//$url/stylesheets/$style/style.css'>";
    return $html;
}
add_shortcode( 'sermon_list', 'sermon_list_html' );
```

### Available widgets

#### Show a player for a single sermon

```html
<single-sermon sermonId="123"></single-sermon>
```

#### Show latest sermon series 

```html
<series-latest limit="10" inGroup="SundaySchool"></series-latest>
```

#### Show a table with sermons from a series 

```html
<series-table inGroup="SundaySchool" inSeries="Attributes of God"></series-table>
```

#### Show a table with sermons

```html
<sermon-list 
    inGroup="SundaySchool" 
    site="" 
    showDirect="true" 
    showSearchBar="true" 
    language="de">
</sermon-list>
```

### Show a list with sermons 

```html
    <sermon-table 
        inGroup="SundaySermon" 
        inSeries="John" 
        showSearchBar="true" 
        showSeries="true" 
        showScripture="true" 
        showDirect="true" 
        sort="{ date: 'desc' }" 
        language="de">
    </sermon-table>
```

## Management

### Config

If you are not using the ansible scripts for creating the config files, then create `config/params.php` and `config/db.php` on your production system.

See also the templates for the config files: [params.j2](https://gitlab.com/ecg-berlin/vue-sermons/-/blob/master/config/params.j2) and [db.j2](https://gitlab.com/ecg-berlin/vue-sermons/-/blob/master/config/db.j2)

Example of a `params.php`

```php
<?php
return [
    'covers' => '//example.com/downloads/hellersdorf/covers',
    'channelLink' => 'https://example.com',
    'channelSubtitle' => 'Subtitle',
    'channelDescription' => 'Description',
    'channelName' => 'Example',
    'channelEMail' => 'example@example.com',
    'defaultCover' => 'https://example.com/favicon.png',
    'copyright' => 'Example',
    'secret' => 'HSIU!nmx!)(§N',
    'users' => [
        ['username' => 'test', 
        'passwordHash' => '$6$'],
    ],
    'groups' => [
        'SundaySchool' => [
            'de' => [
                'title' => 'Title',
                'link' => 'https://example.com/',
                'subtitle' => 'Subtitle',
                'description' => 'Description',
            ],
            'ru' => [
                'title' => 'Title',
                'link' => 'https://example.ru/',
                'subtitle' => 'Subtitle',
                'description' => 'Description',
            ],
        ],
    ]
];
```

