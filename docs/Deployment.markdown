# Deployment

To deploy we need just the configuration files in ansible.

Because this project is used in production, we have a separate repo with the config files. 
In our CI/CD Pipeline we check this repo with the config files out.
