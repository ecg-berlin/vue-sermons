# Ansible

## How to test j2

We use jinja2 to generate config files.

An example from `setup.yml`

```yml
- name: Copy params
  template:
    src: ../config/params.j2
    dest: "{{sermons_api_path}}/config/params.php"
```

To test the generation to php we can use: https://ansible.sivel.net/test/